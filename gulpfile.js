var gulp = require('gulp');

var minifyCss = require('gulp-minify-css');

var sass = require('gulp-sass');

var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');

var notify = require('gulp-notify');

var browserSync = require('browser-sync');

var reload = browserSync.reload;

var connectPHP = require('gulp-connect-php');

// For ES6 Javascript
var jsSrc = './app/js/index.js';
var jsDist = './app/dist/';
var jsWatch = './app/*.js';
var jsFiles = [jsSrc];
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var glob = require('glob');
var es = require('event-stream');
var through = require('through2');


// ////////////////////////////////////////////////
// Paths to source files
// paths haves changed a bit.
// ///////////////////////////////////////////////

var paths = {
  html: ['./app/**/*.php'],
  css: ['./app/styles/**/*.scss'],
  script: ['./app/js/**/*.js']
};

gulp.task('mincss', function () {

  var processors = [
    autoprefixer,
    // flexibility // not working properly
  ];

  return gulp.src(paths.css)

    .pipe(sass().on('error', sass.logError))

    .pipe(postcss(processors))

    // .pipe(minifyCss())

    .pipe(gulp.dest('app/css'))

    .pipe(reload({ stream: true }));

});


// ////////////////////////////////////////////////
// HTML task
// ///////////////////////////////////////////////

gulp.task('html', function () {
  gulp.src(paths.html)
    .pipe(reload({ stream: true }));
});


// ////////////////////////////////////////////////
// Browser-Sync Tasks
// // /////////////////////////////////////////////

gulp.task('browserSync', function () {
  browserSync({
    injectChanges: true,
    proxy: '127.0.0.1/cnc/app',
    port: 8080
  });
});


// /////////////////////////////////////////////////
// PHP task
// ////////////////////////////////////////////////

gulp.task('php', function () {
  connectPHP.server({ base: './', keepalive: true, hostname: 'localhost', port: 8080, open: false });
});

// Bundle version
gulp.task('scripts', function (done) {
  var bundledStream = through();

  bundledStream
    .pipe(source('app.js'))
    .pipe(rename({ extname: '.bundle.js' }))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(uglify())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(jsDist));
  // .pipe(reload({ stream: true }));

  glob('./app/js/**/*.js', function (err, entries) {
    if (err) done(err);
    var b = browserify({
      entries: entries,
      debug: true,
      transform: [[babelify, {
        presets: ['env']
      }]]
    });
    b
      .bundle()
      .pipe(bundledStream);
    return bundledStream;
  });

  // Workaround
  return gulp.src(['./app/dist/*.js'])
    .pipe(reload({ stream: true }));

});

// Simple, no-bundle version
gulp.task('scripts', function () {

  return gulp.src(paths.script)
    .pipe(reload({ stream: true }));

});

gulp.task('watcher', function () {

  gulp.watch(paths.css, ['mincss']);

  // Note: no es6 to compile
  // gulp.watch(paths.script, ['scripts']);
  gulp.watch(paths.script, ['scripts']);

  gulp.watch(paths.html, ['html']);

});


// gulp.task('default', ['watcher', 'browserSync', 'php']);
gulp.task('default', ['watcher', 'browserSync']);
