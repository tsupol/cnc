<?php
$asset_path = "./";
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
  <meta name="description" content="AP Honda - Race to the Dream">
  <title>AP Honda | Race to the Dream</title>
  <link rel="canonical" href="http://aphonda.artplore.com"/>
  <link rel="stylesheet" href="fonts/ap-honda/stylesheet.css" type="text/css" media="all"/>
  <link rel="stylesheet" type="text/css" media="all"
        href="<?php echo $asset_path ?>css/main.css?<?php echo time(); ?>"/>

  <!----================---->

  <style type="text/css">
    .parallaxParent {
      height: 100vh;
      overflow: hidden;
    }

    .parallaxParent > * {
      height: 200%;
      position: relative;
      /*top: -100%;*/
      top: -50%;
    }

    .top-most {
      background-color: cadetblue;
      width: 100%;
      height; 20px;
    }
  </style>

</head>
<body>

<!----================---->

<div class="top-most"></div>

<div class="spacer s0"></div>

<div id="parallax1" class="parallaxParent">
  <div style="background-image: url(./imgs/asdf.jpg);"></div>
</div>

<div class="spacer s1">
  <div class="box2 blue">
    <p>Content 1 2</p>
    <a href="#" class="viewsource">view source</a>
  </div>
</div>

<div class="spacer s0"></div>
<div id="parallax2" class="parallaxParent">
  <div style="background-image: url(./imgs/asdf.jpg);"></div>
</div>
<div class="spacer s1">
  <div class="box2 blue">
    <p>Content 2</p>
    <a href="#" class="viewsource">view source</a>
  </div>
</div>
<div class="spacer s0"></div>
<div id="parallax3" class="parallaxParent">
  <div style="background-image: url(./imgs/asdf.jpg);"></div>
</div>
<div class="spacer s2"></div>

<!----================---->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/skrollr/0.6.30/skrollr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.6.15/browser-polyfill.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/debug.addIndicators.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/animation.gsap.min.js"></script>

<script src="./js/index.js?<?php echo time(); ?>"></script>
<script>
  // init controller
  var controller = new ScrollMagic.Controller({ globalSceneOptions: { triggerHook: "onEnter", duration: "200%" } });

  // build scenes
  new ScrollMagic.Scene({ triggerElement: "#parallax1" })
    .setTween("#parallax1 > div", { y: "100%", ease: Linear.easeNone })
    .addIndicators()
    .addTo(controller);

  // new ScrollMagic.Scene({triggerElement: "#parallax2"})
  //   .setTween("#parallax2 > div", {y: "80%", ease: Linear.easeNone})
  //   .addIndicators()
  //   .addTo(controller);
  //
  // new ScrollMagic.Scene({triggerElement: "#parallax3"})
  //   .setTween("#parallax3 > div", {y: "80%", ease: Linear.easeNone})
  //   .addIndicators()
  //   .addTo(controller);
</script>
</body>
</html>


