<?php
$asset_path = "./";
?>
<!DOCTYPE html>
<html>
<head>
  <!-- Google Tag Manager -->
  <script>(function (w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start':
          new Date().getTime(), event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-N5TFQRZ');</script>
  <!-- End Google Tag Manager -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
  <meta name="description" content="Code & Craft">
  <title>Code & Craft</title>
  <link rel="canonical" href="http://cnc.artplore.com"/>
  <link rel="stylesheet" href="fonts/db_helvethaicax/stylesheet.css" type="text/css" media="all"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css" type="text/css" media="all"/>
  <link rel="stylesheet" type="text/css" media="all"
        href="<?php echo $asset_path ?>css/main.css?<?php echo time(); ?>"/>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
  <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N5TFQRZ"
          height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<div id="main-nav">
  <div class="nav-bg"></div>
  <div class="nav-max">
    <div class="nav-logo-wrap">
      <img class="nav-logo" src="./imgs/logo_cnc.png"/>
    </div>
    <div class="nav-list">
      <a class="nav-item" href="#sec-main">Home</a>
      <a class="nav-item" href="#sec-service">Services</a>
      <a class="nav-item" href="#sec-work">Works</a>
      <a class="nav-item" href="#sec-client">Client</a>
      <a class="nav-item" href="#sec-about">About us</a>
      <a class="nav-item" href="#">Career</a>
      <a class="nav-item" href="#sec-contact">Contact</a>
      <a class="nav-item" href="#">Blog</a>
      <a class="nav-item __search search-trigger">
        <img src="./imgs/icon-search.png"/>
      </a>
      <a class="nav-item __lang-th">
        <div>TH</div>
      </a>
    </div>
  </div>
  <!--  mobile -->
  <div class="nav-mini">
    <div id="nav-toggle"><span></span></div>
    <img class="nav-logo" src="./imgs/logo_cnc.png"/>
    <a class="nav-item __search-m search-trigger">
      <img src="./imgs/icon-search.png"/>
    </a>
  </div>
</div>
<div id="nav-expanded">
  <div class="_inner">
    <a class="nav-item" href="#sec-main">Home</a>
    <a class="nav-item" href="#sec-service">Services</a>
    <a class="nav-item" href="#sec-work">Works</a>
    <a class="nav-item" href="#sec-client">Client</a>
    <a class="nav-item" href="#sec-about">About us</a>
    <a class="nav-item" href="#">Career</a>
    <a class="nav-item" href="#sec-contact">Contact</a>
    <a class="nav-item" href="#">Blog</a>
  </div>
</div>
<div id="search-popup">
  <div class="_inner">
    <input id="search_text" type="text" placeholder="Search Code & Craft"/>
    <div class="_icon"><img src="./imgs/icon-search.png"/></div>
  </div>
</div>

<div id="debug-log"></div>

<!-- Begin -->
<!-- Section - Main -->
<div class="sec-wrap __absolute">
  <div id="sec-main" class="scene-1">
    <!--  <div class="para-item __ref"></div>-->

    <!-- desktop -->
    <div class="hide-md">
      <div class="para-item __bg"></div>
      <img class="para-item __meteor2" src="./imgs/meteorite-2.png"/>
      <img class="para-item __meteor1" src="./imgs/meteorite-1.png"/>
      <img class="para-item __meteor0" src="./imgs/meteorite-0.png"/>
      <h2 class="para-item __code-clam">CODE CALM</h2>
      <h2 class="para-item __and">&amp;</h2>
      <h2 class="para-item __craft">CRAFT</h2>
      <h2 class="para-item __on">ON</h2>
      <img class="para-item __astronaut" src="./imgs/astronaut.png"/>
    </div>

    <!-- mobile -->
    <div class="show-md">
      <h2 class="__title">
        CODE CALM<br/>
        <span class="__bottom">& CRAFT ON</span>
      </h2>
      <img class="__astronaut" src="./imgs/astronaut-m.png"/>
      <img class="__meteor-m" src="./imgs/meteorite-0-m.png"/>
    </div>

  </div>
</div>

<div id="scroll-down">
  <div id="blue-text" class="flex-col-center">

    <!-- desktop -->
    <div class="hide-md _text">
      We also believe that a good digital product should have these three things
    </div>
    <div class="hide-md _text">
      combined technology, art, humanity.
    </div>

    <!-- mobile -->
    <div class="show-md _text">
      We also believe that a good digital product should have<br/>
      these three things combined technology, art, humanity.
    </div>

  </div>
  <div class="_scroll-text">SCROLL DOWN</div>
  <div class="_line __top-line"></div>
  <div class="_line __bottom-line"></div>
</div>

<!-- Section - Services -->
<div class="sec-wrap">
  <div id="sec-service">
    <div class="_bg-space"></div>

    <!-- reference -->
    <!--<div class="_ref-service"></div>-->

    <!-- Service - top most sub-section -->
    <div class="_excerpt flex-col-center">
      <img class="hide-md __meteor-s-top" src="./imgs/meteor-service.png"/>
      <div class="__title-service">
        <div>
          SERVICE<span class="hide-sm">S</span>
        </div>
      </div>

      <!-- desktop -->
      <div class="hide-sm  __text">
        Not only being just a digital product developer, we also help you reaching your digital purpose.<br/>
      </div>
      <div class="hide-sm __text">
        Not only you, but we take care of your clients too.
      </div>
      <!-- mobile -->
      <div class="show-sm __text-m">
        Not only being just a digital product developer,<br/>
        we also help you reaching your digital purpose.<br/>
        Not only you,<br/>
        but we take care of your clients too.
      </div>

    </div>
    <div class="falling-star __fs-1"></div>

    <!-- Service - middle sub-section (service titles) -->
    <!-- desktop -->
    <div id="service-trigger" class="service-wrap hide-md">
      <h3 class="__ux color1">
        UX/Usability<br/>Research
      </h3>
      <h3 class="__seo">
        SEO<br/><span class="_sub-title">Search Engine Optimization</span>
      </h3>
      <h3 class="__web-dev color1">
        WEBSITE<br/><span class="_sub-title">DEVELOPMENT</span>
      </h3>
      <h3 class="__ui">
        UI DESIGN
      </h3>
      <h3 class="__webc">
        WEB<span class="_sub-title">CREATIVE</span>
      </h3>
      <h3 class="__app">
        APPLICATION DEVELOPMENT
      </h3>
      <h3 class="__ecom">
        E-Commerce Service
      </h3>
      <h3 class="__crm color1">
        CRM<br/><span class="_sub-title">Customer Relationship Management</span>
      </h3>
      <img class="__astronaut-2" src="./imgs/astronaut-2.png"/>
      <div class="falling-star __fs-2"></div>
      <div class="falling-star __fs-3"></div>
      <div class="falling-star __fs-4"></div>
    </div>
    <!-- mobile -->
    <div class="service-wrap-m show-md">
      <h3 class="_m_seo">
        SEO<br/><span class="_sub-title">Search Engine Optimization</span>
      </h3>
      <h3 class="_m_ux color1">
        UX/Usability<br/>Research
      </h3>
      <h3 class="_m_web-dev color1">
        WEBSITE<br/><span class="_sub-title">DEVELOPMENT</span>
      </h3>
      <h3 class="_m_ui">
        UI DESIGN
      </h3>
      <div class="_m_webs">
        WEB
        <div class="">
          <div class="_ad">APPLICATION DEVELOPMENT</div>
          <div class="_cre">CREATIVE</div>
        </div>
      </div>
      <h3 class="_m_ecom">
        E-Commerce Service
      </h3>
      <h3 class="_m_crm color1">
        CRM<br/><span class="_sub">Customer Relationship Management</span>
      </h3>
    </div>
  </div>

  <!-- Service - bottom sub-section (service description) -->
  <div class="service-2-wrap">
    <div id="sec-service-2">
      <h4>
        <span class="hide-md">
          Help your work successfully reach its requirement.<br/>
          Not only your work that we care, we are also willing to try our best to expand<br/>
          our services’ limitation in order to satisfy your target’s need.
        </span>
        <span class="show-md">
          Help your work successfully reach its<br/>
          requirement. Not only your work that we care,<br/>
          we are also willing to try our best to expand<br/>
          our services’ limitation in order
        </span>
      </h4>
      <!-- desktop -->
      <img class="hide-md __meteor-s0" src="./imgs/meteor-s0.png"/>
      <img class="hide-md __meteor-s1" src="./imgs/meteor-s1.png"/>
      <img class="hide-md __meteor-s2" src="./imgs/meteor-s2.png"/>
      <!-- mobile -->
      <img class="show-md __meteor-s0-m" src="./imgs/meteor-s0-m.png"/>
      <img class="show-md __meteor-s1-m" src="./imgs/meteor-s1-m.png"/>
    </div>
  </div>
</div>

<!-- Section - Works -->
<div id="sec-work">
  <h2>
    A collection of works
    <span class="__light"><br class="show-sm"/>we have created.</span>
  </h2>

  <!-- Carousel -->
  <div id="work-carousel">
    <a class="wc-item"><img src="./imgs/works/ais-best-network.jpg"/></a>
    <a class="wc-item"><img src="./imgs/works/honda-crv-anywhere.jpg"/></a>
    <a class="wc-item"><img src="./imgs/works/ap-honda.jpg"/></a>
    <a class="wc-item"><img src="./imgs/works/ap-honda-bigbike.jpg"/></a>
    <a class="wc-item"><img src="./imgs/works/ap-honda-dealer-hub.jpg"/></a>
    <a class="wc-item"><img src="./imgs/works/honda-crv.jpg"/></a>
    <a class="wc-item"><img src="./imgs/works/honda-dealer-hub.jpg"/></a>
    <a class="wc-item"><img src="./imgs/works/malee.jpg"/></a>
    <a class="wc-item"><img src="./imgs/works/mini-yours-edition.jpg"/></a>
    <a class="wc-item"><img src="./imgs/works/scb-business-anywhere.jpg"/></a>
    <a class="wc-item"><img src="./imgs/works/ap-honda-line-bcrm.jpg"/></a>
  </div>

  <p class="hide-sm _desc">Here are parts of work that we and our clients worked together. We hope it would make you feel assured in our services.</p>
  <p class="show-sm _desc">We and our clients worked together. We hope it<br/> would make you feel assured in our services.</p>
</div>

<!-- Section - Clients -->
<div class="sec-wrap">
  <div id="sec-client" class="flex-col-center">
    <div class="_v-line"></div>
    <h2>Some of our Awesome Clients</h2>
    <div class="clients">
      <a href="#" class="_client">
        <img src="./imgs/clients/scb.png"/>
      </a>
      <a href="#" class="_client">
        <img src="./imgs/clients/qsmt.png"/>
      </a>
      <a href="#" class="_client">
        <img src="./imgs/clients/honda.png"/>
      </a>
      <a href="#" class="_client">
        <img src="./imgs/clients/ais.png"/>
      </a>
      <a href="#" class="_client">
        <img src="./imgs/clients/mangozero.png"/>
      </a>
      <a href="#" class="_client">
        <img src="./imgs/clients/mk.png"/>
      </a>
      <a href="#" class="_client">
        <img src="./imgs/clients/honda2.png"/>
      </a>
      <a href="#" class="_client">
        <img src="./imgs/clients/krua.png"/>
      </a>
      <a href="#" class="_client">
        <img src="./imgs/clients/crv.png"/>
      </a>
      <a href="#" class="_client">
        <img src="./imgs/clients/ais-live.png"/>
      </a>
    </div>
    <img class="_hand __hand-left" src="./imgs/hand-left.png"/>
    <img class="_hand __hand-right" src="./imgs/hand-right.png"/>
  </div>
</div>

<!-- Section - About -->
<div id="sec-about">
  <div class="_wrap flex-center">
    <div class="_craft-on">Get your Craft on!</div>
    <img class="_astronaut-3" src="./imgs/astronaut-3.png"/>
  </div>
</div>

<!-- Section - Contact -->
<div id="sec-contact">
  <div class="contact-detail">
    <div class="_inner">
      <h2>C<span class="color1">O</span>NTACT;</h2>
      <h4>Code & Craft Co.,Ltd.</h4>
      <h5>A Subsidiary of Rabbit Digital Group</h5>
      <p class="_addr">
        Rabbits Digital Group, 1706/34, Rama VI Road,<br/>
        Rongmuang, Pathumwan, Bangkok 10330
      </p>
      <div class="_contact">
        <img class="__mail" src="./imgs/icon-mail.png"/>
        jump@rabbitstale.com
      </div>
      <div class="_contact">
        <img class="__tel" src="./imgs/icon-tel.png"/>
        <span>(+66) 2219 23857<span>
      </div>
      <a href="#" class="_button">
        Get in touch get to started
      </a>
      <p class="_outro">
        Coded and crafted with love by
        <b class="color1">Code & Craft</b>
      </p>
    </div>
  </div>
  <div class="cnc-map"></div>
</div>

<!-- Footer -->
<div id="footer" class="flex-center">
  <img class="_logo-f" src="./imgs/logo_cnc.png"/>
  <div class="_cp">© 2018. All Rights Reserved</div>
</div>

<div id="back-to-top" class="flex-col-center">
  <img class="_img" src="./imgs/btt.png"/>
  <div class="_label">BACK TO TOP</div>
</div>

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.6.15/browser-polyfill.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.2/TweenMax.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/debug.addIndicators.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/animation.gsap.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>

<script src="./js/index.js?<?php echo time(); ?>"></script>
<script src="./js/slideNav.js"></script>
</body>
</html>


